package com.example.billiard;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.View;
import android.widget.*;
import android.content.DialogInterface;
import android.content.Context;


public class MainActivity extends Activity {

	private GameTable tbl;
    private static final int DIALOG_GET_COUNT_ID = 1;
    private static final int DIALOG_GAMEOVER_ID = 2;
    private int countDestroyed = 0;
    Handler handler = new Handler();


    private class GameLoop implements Runnable {

        @Override
        public void run() {
            try {
                gameLoop();
            }catch (InterruptedException exception){
                exception.printStackTrace();
                Thread.currentThread().interrupt();
            }
        }
    }

    private void gameLoop() throws InterruptedException {
        while (tbl.isRunning()){
            tbl.runLoop();
        }
        if (!tbl.isRunning()) {
            countDestroyed = tbl.getScore();
            showPoints();
        }
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        Dialog dialog;
        switch(id) {
            case DIALOG_GET_COUNT_ID:
                dialog = showStartDialog();
                break;
            default:
                dialog = null;
        }
        return dialog;
    }

    private void showPoints(){
        handler.post(new Runnable() {
            public void run() {
                StringBuffer message = new StringBuffer();
                message.append(getString(R.string.text_final)).append(" - ").append(countDestroyed);
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                initExitButton();
            }
        });



    }

    private void initExitButton() {
        Button selfButton = (Button) findViewById(R.id.go_button);
        selfButton.setText(R.string.exit_game);
        selfButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        int width = displaymetrics.widthPixels;
        tbl = new GameTable(this, width, height);
        showDialog(DIALOG_GET_COUNT_ID);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
    
    public void startAction(View view) {
    	Button selfButton = (Button) findViewById(R.id.go_button);
    	selfButton.setText(R.string.button_go);
    	tbl.startGame();
	}
    
    private Dialog showStartDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
    	final EditText txt = new EditText(this);
    	txt.setText(R.string.default_count);
        builder
        .setTitle(R.string.get_count)
        .setMessage(R.string.get_count)
        .setView(txt)
        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            	Editable value = txt.getText(); 
            	String count = value.toString();
                setContentView(R.layout.activity_main);
            	LinearLayout container = (LinearLayout) findViewById(R.id.table_container);
            	tbl.initManager(Integer.valueOf(count));
            	container.addView(tbl);
                tbl.prepareGame();
                startGameThread();
            }
        });
        return builder.create();
	}

    private void startGameThread() {
        Runnable r = new GameLoop();
        Thread t = new Thread(r);
        t.start();
    }


}
