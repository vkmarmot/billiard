package com.example.billiard;

import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.graphics.drawable.shapes.Shape;

public class Boll {
	
	private Drawable bImage;
	private float BasePercent = 0.2f;
    private float coefficient = 0.9f;
	private int bDiametr = 30;
    protected Point bPoint;
	private int bAngle;
	private int bSpeed;
	private static final int DEFAULT_SPEED = 0;
    public static final int PI = 180;
    private boolean started = false;
    private static final int STEP_TO_FADE_OUT = 50;
    private Random rnd;
    private static int OUT_OF_VERTICAL = 1;
    private static int OUT_OF_HORIZONTAL = -1;
    private int steps = 0;
    private boolean moved = false;
    private int verticalVector = 1;
    private int horisontalVector = 1;
    private boolean checked = false;
    private int color;


    public int generateRandomDiametr() {
        return (int)(bDiametr + rnd.nextInt(bDiametr)*BasePercent - 
        		rnd.nextInt(bDiametr)*BasePercent);
    }

    public boolean isMovedBoll(){
        return moved;
    }

    public void setMovedBoll(boolean moved) {
        this.moved = moved;
    }

    
    public void setChecked(boolean status) {
    	checked = status;
	}
    
    public int getDiametr() {
		return bDiametr;
	}

    public boolean isChecked() {
    	return checked;
	}
    public boolean isClicked(float left, float top) {
    	return isPointInBall(left, top);
	}
    
    public float getCenterY() {
    	float radius = bDiametr / 2;
		return bPoint.y + radius;
	}
    
    public float getCenterX() {
    	float radius = bDiametr / 2;
		return bPoint.x + radius;
	}
    
    public boolean isPointInBall(float left, float top) {
    	float vert = Math.abs(top - getCenterY());
    	float hor = Math.abs(left - getCenterX());
    	double gip = Math.sqrt(hor*hor + vert*vert);
		return gip<(bDiametr/2);
	}
    public void setMoving(boolean option) {
		started = option;
	}
    
    public void changeColor() {
    	((ShapeDrawable) bImage).getPaint().setColor(
        		getRandomColor());
	}

	private static int[] colors = {Color.BLUE, Color.RED, Color.GRAY}; 
	public Boll(int defaultleft, int defaultTop, int bDiametr, Random rnd) {
    	ShapeDrawable activeDrawable = new ShapeDrawable();
    	this.bDiametr = bDiametr;
        activeDrawable.setBounds(0, 0, (int) bDiametr,
                (int) bDiametr);
        this.rnd = rnd;
        ((ShapeDrawable) activeDrawable).getPaint().setColor(
        		getRandomColor());
    	Shape shape = new OvalShape();
    	shape.resize(bDiametr, bDiametr);
    	((ShapeDrawable) activeDrawable).setShape(shape);
		bImage = activeDrawable;
        bPoint = new Point(defaultleft, defaultTop);
        bSpeed = DEFAULT_SPEED; // ������ �������� �� ���������
        bAngle = getRandomAngle(); // ������ ��������� ��������� ����
		
	}
	
	private int getRandomAngle()
    {
		int fullAngle = PI;
        return (rnd.nextInt(fullAngle));
    }

    public int getHorisontalVector() {
        return horisontalVector;
    }

    public int getVerticalVector() {

        return verticalVector;
    }

    protected void updatePoint(float containerLeft, float containerTop, float containerWidth, float containerHeight)
    {
        int correct = correctBounds(containerLeft, containerTop, containerWidth, containerHeight);
        if(correct != 0){
        	if(correct == OUT_OF_HORIZONTAL){
                horisontalVector = -horisontalVector;
        	}else{
                verticalVector = -verticalVector;
        	}
        }
        bPoint.x = getNewX();
        bPoint.y = getNewY();
        updateSpeed();
    }

    private boolean needUpdateSpeed(){
        steps += 1;
        return steps > STEP_TO_FADE_OUT;
    }

    private void updateSpeed() {
        if (needUpdateSpeed()) {
            bSpeed *= coefficient;
            steps = 0;
        }
        setMovedBoll(bSpeed>0);
    }

    public int getColor() {
		return color;
	}
	
	public int getProectionX() {
        double angle = Math.toRadians(bAngle);
		return (int)Math.round(bSpeed * Math.cos(angle))*horisontalVector;
	}
	
	public int getProectionY() {
        double angle = Math.toRadians(bAngle);
		return (int)Math.round(bSpeed * Math.sin(angle))*verticalVector;
	}



	public int getNewY() {
		return bPoint.y + getProectionY();
	}


	public int getNewX() {
		return bPoint.x + getProectionX();
	}
	


	public int getY() {
		return bPoint.y;
	}


	public int getX() {
		return bPoint.x;
	}
	
	private int correctBounds(float containerLeft, float containerTop,
			float containerWidth, float containerHeight) {
		if(containerLeft > bPoint.x ||
				containerLeft + containerWidth <bPoint.x + bDiametr ){
			int nextX = getNewX();
			if(Math.abs(nextX - containerLeft - containerWidth/2) > 
				Math.abs(bPoint.x - containerLeft - containerWidth/2)){
				return OUT_OF_HORIZONTAL;
			}
		}else if(containerTop > bPoint.y ||
				containerTop + containerHeight < bPoint.y + bDiametr){
			int nextY = getNewY();
			if(Math.abs(nextY - containerTop - containerHeight/2) > 
				Math.abs(bPoint.y - containerTop - containerHeight/2)){
				return OUT_OF_VERTICAL;
			}
		}
		return 0;
	}
	
	public boolean goToDifferent(Boll boll) {
		return false;
	}

	public void update(float containerLeft, float containerTop, float containerWidth, float containerHeight)
    {
		if(started) {

            if (isMovedBoll()) {
                updatePoint(containerLeft, containerTop, containerWidth, containerHeight);
            }
        }
        bImage.setBounds(bPoint.x, bPoint.y, bPoint.x + bDiametr, bPoint.y + bDiametr);
    }
	
	public int getRandomColor() {
		color = colors[rnd.nextInt(colors.length)];
		return color;
	}
	public void draw(Canvas canvas)
    {
        bImage.draw(canvas);
    }
	
	public void setLeft(int value)
    {
        bPoint.x = value;
    }

    public void setRight(int value)
    {
    	bPoint.x = value - bDiametr;
    }
    
    public void setTop(int value)
    {
    	bPoint.y = value;
    }

    public void setBottom(int value)
    {
    	bPoint.y = value - bDiametr;
    }
    
    public void setCenterX(int value)
    {
    	bPoint.x = value - bDiametr / 2;
    }
    
    public void setCenterY(int value)
    {
    	bPoint.y = value - bDiametr / 2;
    }
    
    
    public int getAngle() {
		return bAngle;
	}
    
    public int getSpeed() {
		return bSpeed;
	}
    
    public void setSpeed(int speed) {
		bSpeed = speed;
	}
    
    public void setAngle(int angle) {
		bAngle = angle;
	}

	public void pushBoll() {
        bSpeed = (DEFAULT_SPEED + 10)+(rnd.nextInt(10));
        bAngle = getRandomAngle(); // ������ ��������� ��������� ����
        setMovedBoll(true);
	}

    public void moveTo(float left, float top) {
        setCenterX((int)left);
        setCenterY((int)top);
    }
}
