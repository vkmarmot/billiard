package com.example.billiard;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

public class GameTable  extends SurfaceView implements SurfaceHolder.Callback  {

    private SurfaceHolder gameTable;
    private GameManager mThread;
    private float topClick;
    private float leftClick;
    private boolean moved = false;
    private float eventTimeStart;
    private float eventTimeMsToDragStart = 600;

    public boolean isRunning() {
        return mThread.getRunning();
    }

    public int getScore() {
        return mThread.getScore();
    }

    private class onTouchCanvas implements OnTouchListener {

		public boolean onTouch(View v, MotionEvent event) {
            int motionType = event.getActionMasked();
            float left = event.getX();
            float top = event.getY();
            float eventTime = event.getEventTime();
            if (motionType == MotionEvent.ACTION_DOWN) {
                eventTimeStart = eventTime;
                leftClick = left;
                topClick = top;
                moved = false;
                mThread.prepareToMove(left, top);
            }else if(motionType == MotionEvent.ACTION_MOVE){
                if (isTimeToMove(eventTime)) {
                    mThread.SurfaceMoved(left, top);
                    moved = true;
                }
            }else if(motionType == MotionEvent.ACTION_UP){
                if (!moved) {
                    mThread.SurfaceClicked(leftClick, topClick);
                }
            }
            return true;
		}

        private boolean isTimeToMove(float eventTime) {
            return eventTime - eventTimeStart > eventTimeMsToDragStart;
        }

    }

    public void runLoop() throws InterruptedException{
        mThread.run();
    }
    
    public GameTable(Context context, int availableWidth, int availableHeight)
    {
        super(context);

        gameTable = getHolder();
        gameTable.addCallback(this);
        this.setOnTouchListener(new onTouchCanvas());
        mThread = new GameManager(gameTable, context, availableWidth, availableHeight);
    }
    public void initManager(int count) {
    	mThread.initWithCount(count);
	}
    

	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
	    mThread.initPositions(height, width);
		
	}
	

	public void surfaceCreated(SurfaceHolder holder) {
		
	}
	
	public void prepareGame() {
		mThread.setRunning(true);
	}

	public void startGame() {
		mThread.setGaming(true);
	}

	public void surfaceDestroyed(SurfaceHolder holder) {
		boolean retry = true;
	    mThread.setRunning(false);
	    while (retry)
	    {
	        try
	        {
	            // ожидание завершение потока
	            retry = false;
	        }
	        catch (Exception e) { }
	    }
		
	}
	
	

	

}
