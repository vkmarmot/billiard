package com.example.billiard;

import java.util.ArrayList;
import java.util.Random;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.view.SurfaceHolder;

public class GameManager
{
    private static final int FIELD_WIDTH = 300;
    private static final int FIELD_HEIGHT = 250;

   /** �������, �� ������� ����� �������� */
    private SurfaceHolder mSurfaceHolder;
    private Bitmap mBackground;
    
    private ArrayList<Boll> bolls;

    /** ��������� ������ (����������� ��� ���. �����, ����� ���� ������� ��������� �����, ����� �����������) */
    private boolean mRunning;
    private boolean mMoving;
    private int countDestroyed = 0;

    /** ����� ��������� */
    private Paint mPaint;

    /** ������������� �������� ���� */
    private Rect mField;
    private int left = 10;
    private int top = 50;
    private Random rnd = new Random(System.currentTimeMillis());
    public static final String COUNT_MESSAGE = "com.example.billiard.COUNT_MESSAGE";
    private Boll movingBoll = null;

    /**
     * �����������
     * @param surfaceHolder ������� ���������
     * @param context �������� ����������
     */
    public GameManager(SurfaceHolder surfaceHolder, Context context, int availableWidth, int availableHeight)
    {
        bolls = new ArrayList<Boll>();
        mSurfaceHolder = surfaceHolder;
        mRunning = false;
        mMoving = false;

        mPaint = new Paint();
        mPaint.setColor(Color.BLUE);
        mPaint.setStrokeWidth(2);
        mPaint.setStyle(Style.STROKE);

        left = (availableWidth - FIELD_WIDTH) / 2;
        top = (availableHeight - FIELD_HEIGHT) / 2;
        mField = new Rect(left, top, left + FIELD_WIDTH, top + FIELD_HEIGHT);
    }

    public Boll getClickedBoll(float left, float top){
        for(Boll boll : bolls){
            if(boll.isClicked(left, top)){
                return boll;
            }
        }
        return null;
    }
    
    public void SurfaceClicked(float left, float top) {
        Boll clickedBoll = getClickedBoll(left, top);
        if (clickedBoll != null) {
            if(!mMoving){
                clickedBoll.changeColor();
            }else{
                clickedBoll.pushBoll();
            }
        }
        setMovedBoll(null);
    }

    private void setMovedBoll(Boll clickedBoll) {
        this.movingBoll = clickedBoll;
    }

    public void initWithCount(int countBolls) {
        drawRounds(countBolls);
    }

    /**
     * ������� ��������� ������
     * @param running
     */
    public void setRunning(boolean running)
    {
        mRunning = running;
    }

    public boolean ismMoving() {
        return mMoving;
    }

    public void setGaming(boolean option) {
        mMoving = option;
        for(Boll boll : bolls){
            boll.setMoving(option);
        }
    }

    /** ��������, ����������� � ������ */
    public void run() throws InterruptedException
    {
        boolean lastUpdate = false;
        while (mRunning)
        {
            Canvas canvas = null;
            try
            {
                // ���������� Canvas-�
                canvas = mSurfaceHolder.lockCanvas();
                synchronized (mSurfaceHolder)
                {

                    // ���������� ���������
                    boolean moved = updateObjects(); // ��������� �������
                    refreshCanvas(canvas); // ��������� �����
                    if(lastUpdate && !moved) {
                        mRunning = false;
                    }
                    lastUpdate = moved;
                    Thread.sleep(50);
                }
            }
            catch (Exception e) { }
            finally
            {
                if (canvas != null)
                {
                    mSurfaceHolder.unlockCanvasAndPost(canvas);
                }
            }
        }
    }


    private void drawRounds(int roundsCount) {
        int leftBoll = mField.left;
        int topBoll = mField.top;
        int bollPadding = 10;
        int bDiametr = 30;
        int diametr;
        float percent = 0.2f;
        for(int i = 0; i < roundsCount; i += 1) {
            diametr = generateDiametr(bDiametr, percent);
            boolean hasSpaceForBoll = topBoll + diametr < FIELD_HEIGHT + mField.top;
            if (hasSpaceForBoll) {
                Boll mBall = new Boll(leftBoll, topBoll, diametr, rnd);
                bolls.add(mBall);
                leftBoll += diametr + bollPadding;

                boolean needDrawInNextLine = leftBoll + diametr > FIELD_WIDTH;
                if(needDrawInNextLine){
                    leftBoll = left;
                    topBoll += diametr + bollPadding;
                }
            }
        }

    }

    private int generateDiametr(int bDiametr, float percent) {
        return (int)(bDiametr + rnd.nextInt(bDiametr)* percent -
                rnd.nextInt(bDiametr)* percent);
    }

    public void initPositions(int screenHeight, int screenWidth)
    {
        left = (screenWidth - FIELD_WIDTH) / 2;
        top = (screenHeight - FIELD_HEIGHT) / 2;

        mBackground = Bitmap.createBitmap(screenWidth, screenHeight, Bitmap.Config.RGB_565);
        mField.set(left, top, left + FIELD_WIDTH, top + FIELD_HEIGHT);
    }
    private void refreshCanvas(Canvas canvas)
    {
        canvas.drawBitmap(mBackground, 0, 0, null);
        // ������ ������� ����
        canvas.drawRect(mField, mPaint);

        // ������ ������� �������
        for(Boll boll : bolls){
            boll.draw(canvas);
        }
    }

    /** ���������� ��������� ������� �������� */
    private boolean updateObjects()
    {
        boolean isMoved = false;
         for(Boll boll : bolls){
             boll.update(left, top, FIELD_WIDTH, FIELD_HEIGHT);
            isMoved = isMoved || boll.isMovedBoll();
             boll.setChecked(true);
             if (mMoving) {
                 checkBolls(boll);
             }
         }

         for(Boll boll : bolls){
             boll.setChecked(false);
         }
        return isMoved;
    }
    
    private void checkBolls(Boll currentBoll) {
         float centerCurrentBollX = currentBoll.getCenterX();
         float centerCurrentBollY = currentBoll.getCenterY();
         float currentBollRadius = currentBoll.getDiametr()/2;
        for(Boll boll : bolls){
            if(!boll.equals(currentBoll)){
                if(!boll.isChecked()){
                     float centerBollX = boll.getCenterX();
                     float centerBollY = boll.getCenterY();
                    double gip = getGip(centerCurrentBollX, centerCurrentBollY,
                            centerBollX, centerBollY);
                    if(gip < (boll.getDiametr()/2 + currentBollRadius) && 
                            !currentBoll.goToDifferent(boll)){
                        if(currentBoll.getColor() == boll.getColor()){
                            bolls.remove(boll);
                            bolls.remove(currentBoll);
                            countDestroyed += 2; 
                            return;
                        }
                        
                        double gipnew = getGip(boll.getNewX(), boll.getNewY(),
                                currentBoll.getNewX(), currentBoll.getNewY());
                        if(gipnew < gip){
                            boll.setMovedBoll(true);
                            currentBoll.setMovedBoll(true);
                            recalculateAngles(boll, currentBoll);
                        }
                    }
                 }
            }
             else if(boll.equals(currentBoll)){
                 boll.isChecked();
             }
         }
    }

    private double getGip(float centerCurrentBollX, float centerCurrentBollY,
            float centerBollX, float centerBollY) {
        float vert = Math.abs(centerCurrentBollY - centerBollY);
        float hor = Math.abs(centerCurrentBollX - centerBollX);
        return Math.sqrt(hor * hor + vert * vert);
    }
    
    private void recalculateAngles(Boll boll1, Boll boll2) {
        float x1 = boll1.getCenterX();
        float x2 = boll2.getCenterX();
        float y1 = boll1.getCenterY();
        float y2 = boll2.getCenterY();
        float v1x = boll1.getProectionX();
        float v1y = boll1.getProectionY();
        float v2x = boll2.getProectionX();
        float v2y = boll2.getProectionY();
        float r1 = boll1.getDiametr()/2;
        float r2 = boll2.getDiametr()/2;
        float nx,ny,kx,ky,v1n,v1k,v2n,v2k,cs,sn,v;
        // ��������� ���������� ����������� �������
        nx=x2 - x1;
        ny=y2 - y1;
        // ��������� ���������� ������������ �������
        kx=-ny;
        ky=nx;
        // ������� �������� ��������� �� ���������� � ����������� �������
        v1n = ( v1x * nx + v1y * ny ) / (2 * r1);
        v2n = ( v2x * nx + v2y * ny ) / (2 * r2);
        v1k = ( v1x * kx + v1y * ky ) / (2 * r1);
        v2k = ( v2x * kx + v2y * ky ) / (2 * r2);

        v = v1n;
        v1n = v2n;
        v2n = v;
        // ���������� ��������� ��������
        cs = nx / ( 2 * r1 );
        sn = ny / ( 2 * r1 );
        // ������� �������� ��������� ����� �����
        v1x = v1n * cs - v1k * sn;
        v1y = v1n * sn + v1k * cs;
        v2x = v2n * cs - v2k * sn;
        v2y = v2n * sn + v2k * cs;
        setAngle(boll1, v1x, v1y);
        setAngle(boll2, v2x, v2y);
    }

    private void setAngle(Boll boll1, float v1x, float v1y) {
        boll1.setSpeed((int) Math.sqrt(v1x * v1x + v1y * v1y));
        float angleBoll1 = (float) Math.acos(v1x / boll1.getSpeed());
        boll1.setAngle((int) Math.toDegrees(angleBoll1));
    }


    public boolean getRunning() {
        return mRunning;
    }

    public int getScore() {
        return countDestroyed;
    }

    public void SurfaceMoved(float left, float top) {
        if(!mMoving && movingBoll != null) {
            movingBoll.moveTo(left, top);
        }
    }

    public void prepareToMove(float left, float top) {
        setMovedBoll(getClickedBoll(left, top));
    }
}